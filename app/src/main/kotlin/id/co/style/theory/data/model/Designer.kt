package id.co.style.theory.data.model

data class Designer(val id: String = "", val name: String = "")
