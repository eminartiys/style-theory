package id.co.style.theory

import id.co.style.theory.ui.color.ColorFragment
import id.co.style.theory.ui.product.ProductFragment

interface AppGraph {

    fun inject(app: App)

    fun inject(productFragment: ProductFragment)

    fun inject(colorFragment: ColorFragment)
}