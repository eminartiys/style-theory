package id.co.style.theory.data.api.service

import id.co.style.theory.data.model.Colors
import io.reactivex.Single
import retrofit2.http.GET

interface ColorService {

    @GET("v1/colors")
    fun getColors(): Single<Colors>
}
