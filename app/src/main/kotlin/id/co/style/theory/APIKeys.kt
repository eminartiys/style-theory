package id.co.style.theory

import android.util.Base64
import android.util.Log
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class APIKeys @Inject constructor() {

    companion object {
        init {
            try {
                System.loadLibrary("keys")
            } catch (error: UnsatisfiedLinkError) {
                Log.e("ERROR", error.message)
            }
        }
    }

    private external fun endpointDevelopment(): String



    fun getEndpointDevelopment(): String {
        return String(Base64.decode(endpointDevelopment(), Base64.DEFAULT))
    }
}
