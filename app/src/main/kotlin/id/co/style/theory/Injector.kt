package id.co.style.theory

import android.content.Context

object Injector {

    fun obtain(context: Context) : AppGraph {
        return App.get(context).getInjector()
    }

    internal fun create(app: App): AppGraph {
        return DaggerAppComponent.builder().appModule(AppModule(app)).build()
    }
}
