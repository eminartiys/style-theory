package id.co.style.theory.data.api.config

import com.google.gson.annotations.SerializedName

class RestError(@field:SerializedName("code") var code: Int,
                @field:SerializedName("message") var message: String?) {

    companion object {
        val ERRORNOTFOUND = 404
        val ERRORCONFLICT = 409
        val ERRORINTERNALSERVER = 500
    }
}
