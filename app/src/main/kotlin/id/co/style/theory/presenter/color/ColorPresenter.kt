package id.co.style.theory.presenter.color

import android.util.Log
import id.co.style.theory.repository.color.ColorRepository
import id.co.style.theory.ui.color.ColorViewContract
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ColorPresenter @Inject
    constructor(private val repository: ColorRepository): ColorPresenterContract {

    private lateinit var view: ColorViewContract

    private var disposable: Disposable? = null

    override fun setView(view: ColorViewContract) {
        this.view = view
    }

    override fun loadColors() {
        if (disposable != null) {
            Log.i(TAG, "Still loading feed, cancelling...")
            return
        }

        view.hideError()

        disposable = repository.getColors()
            .subscribe({ colors ->
                dispose()

                view.show(colors.colors)
            }, { throwable ->
                dispose()
                view.showError()

                Log.e(TAG, throwable.message)
            })
    }

    override fun dispose() {
        disposable?.let {
            if (!disposable!!.isDisposed) {
                disposable!!.dispose()
            }
            disposable = null
        }
    }

    companion object {
        private const val TAG = "ProductPresenter"
    }

}