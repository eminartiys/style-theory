package id.co.style.theory.data.model

data class Price(val currency: String = "", val amount: Double = 0.0)
