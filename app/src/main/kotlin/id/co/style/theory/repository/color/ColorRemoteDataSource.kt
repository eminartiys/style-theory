package id.co.style.theory.repository.color

import id.co.style.theory.data.api.service.ColorService
import id.co.style.theory.data.model.Colors
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ColorRemoteDataSource @Inject
    constructor(private val colorService: ColorService): ColorDataSource {

    override fun getColors(): Single<Colors> {
        return colorService.getColors()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}