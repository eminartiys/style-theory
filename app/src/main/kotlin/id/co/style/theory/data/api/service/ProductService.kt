package id.co.style.theory.data.api.service

import id.co.style.theory.data.model.Feed
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductService {

    @GET("v3/products")
    fun getProducts(@Query("per_page") perPage: Int,
                    @Query("page_number") pageNumber: Int,
                    @Query("category") category: Int?,
                    @Query("designer") designer: Int?,
                    @Query("colors") colors: String?): Single<Feed>
}
