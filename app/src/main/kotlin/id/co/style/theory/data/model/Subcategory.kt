package id.co.style.theory.data.model

data class Subcategory(val id: String = "",
                       val name: String = "",
                       val filteringOption: FilterOption = FilterOption())
