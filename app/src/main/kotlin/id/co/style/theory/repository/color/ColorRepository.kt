package id.co.style.theory.repository.color

import id.co.style.theory.data.model.Colors
import io.reactivex.Single
import javax.inject.Inject

class ColorRepository @Inject
    constructor(private val remoteDataSource: ColorRemoteDataSource): ColorDataSource {

    override fun getColors(): Single<Colors> {
        return remoteDataSource.getColors()
    }
}