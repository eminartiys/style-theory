package id.co.style.theory.repository.product

import id.co.style.theory.data.model.Feed
import io.reactivex.Single
import javax.inject.Inject

class ProductRepository @Inject
    constructor(private val remoteDataSource: ProductRemoteDataSource): ProductDataSource {

    override fun getProducts(byPageNumber: Int, category: Int?,
                             designer: Int?, colors: String?): Single<Feed> {
        return remoteDataSource.getProducts(byPageNumber, category, designer, colors)
    }
}