package id.co.style.theory.presenter.color

import id.co.style.theory.ui.color.ColorViewContract

interface ColorPresenterContract {

    fun setView(view: ColorViewContract)

    fun loadColors()

    fun dispose()

}