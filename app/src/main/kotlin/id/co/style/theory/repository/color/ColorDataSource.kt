package id.co.style.theory.repository.color

import id.co.style.theory.data.model.Colors
import io.reactivex.Single

interface ColorDataSource {

    fun getColors(): Single<Colors>
}