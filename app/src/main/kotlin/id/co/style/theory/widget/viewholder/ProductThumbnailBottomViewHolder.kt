package id.co.style.theory.widget.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import id.co.style.theory.GlideApp
import id.co.style.theory.data.model.Product
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product_thumbnail_text_bottom.*
import java.lang.ref.WeakReference

class ProductThumbnailBottomViewHolder
    internal constructor(itemView: View): RecyclerView.ViewHolder(itemView),
    View.OnClickListener, LayoutContainer {

    override val containerView: View
        get() = itemView

    private var item: WeakReference<Product>? = null
    private var itemPosition: Int = 0

    init {
        containerView.setOnClickListener(this)
    }

    fun bindView(product: Any, itemPosition: Int) {
        if (product !is Product) {
            return
        }

        this.item = WeakReference(product)
        this.itemPosition = itemPosition

        if (!product.designer.isNullOrEmpty()) {
            productDesigner.text = product.designer[0].name
        } else {
            productDesigner.text = product.name
        }

        productName.text = product.name
        productPrice.text = product.price.currency.plus(" ${product.price.amount}")

        if (product.featuredImage.isNotEmpty()) {
            GlideApp.with(containerView.context)
                .load(product.featuredImage)
                .centerCrop()
                .dontAnimate()
                .transforms(CenterCrop(), RoundedCorners(CORNER_RADIUS))
                .into(productImage)
        }
    }

    override fun onClick(view: View) {
    }

    companion object {
        private const val CORNER_RADIUS = 10
    }
}
