package id.co.style.theory.data.api.config

import io.reactivex.Single
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type


class RxErrorHandlingCallAdapterFactory: CallAdapter.Factory() {

    private val original by lazy {
        RxJava2CallAdapterFactory.create()
    }

    override fun get(returnType: Type,
                     annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *> {
        val wrapped = original.get(returnType, annotations, retrofit) as CallAdapter<out Any, *>
        return RxCallAdapterWrapper(retrofit, wrapped)
    }

    private class RxCallAdapterWrapper<R>(val retrofit: Retrofit,
                                          val wrappedCallAdapter: CallAdapter<R, *>):
                                            CallAdapter<R, Single<R>> {
        override fun responseType(): Type = wrappedCallAdapter.responseType()

        @Suppress("UNCHECKED_CAST")
        override fun adapt(call: Call<R>): Single<R> {
            return (wrappedCallAdapter.adapt(call) as Single<R>)
                    .onErrorResumeNext { throwable: Throwable ->
                        Single.error(asRetrofitException(throwable))
                    }
        }

        private fun asRetrofitException(throwable: Throwable): RetrofitException {
            return when (throwable) {
                is HttpException -> {
                    val response = throwable.response()

                    RetrofitException.httpError(response.raw().request().url().toString(),
                            response, retrofit)
                }

                is IOException -> RetrofitException.networkError(throwable)
                else -> {
                    // We don't know what happened. We need to simply convert to an unknown error
                    RetrofitException.unexpectedError(throwable)
                }
            }
        }

    }

    companion object {
        fun create(): CallAdapter.Factory = RxErrorHandlingCallAdapterFactory()
    }
}
