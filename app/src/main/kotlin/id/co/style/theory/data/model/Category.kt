package id.co.style.theory.data.model

data class Category(val id: String = "",
                    val name: String = "",
                    val subcategories: List<Subcategory> = ArrayList(),
                    val filteringOption: FilterOption = FilterOption())