package id.co.style.theory.data.api.config

@Target(AnnotationTarget.TYPE)
@Retention(AnnotationRetention.RUNTIME)
annotation class AutoGson
