package id.co.style.theory.widget.contract

import id.co.style.theory.data.model.Color

interface ItemClickListener {

    fun onColorClicked(color: Color)
}