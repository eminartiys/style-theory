package id.co.style.theory.widget.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import id.co.style.theory.util.ViewUtils
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_load_grid_two_skeleton.*

class LoadingGridTwoViewHolder internal constructor(itemView: View): RecyclerView.ViewHolder(itemView),
        LayoutContainer {

    override val containerView: View
        get() = itemView

    fun bindView() {
        ViewUtils.skeletonLoading(shimmerFrameLayout, true)
    }
}
