package id.co.style.theory.util

class StringUtils {

    companion object {
        fun isBlank(string: CharSequence?): Boolean {
            return string == null || string.toString().isEmpty() || string.toString().trim().isEmpty()
        }
    }
}
