package id.co.style.theory.data.api.config

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor

class ApiConfig {

    companion object {
        private const val MIME_APPLICATION_JSON = "application/json"
        private const val ACCEPT_TYPE = MIME_APPLICATION_JSON
        private const val CONTENT_TYPE = MIME_APPLICATION_JSON

        private var sharedGson: Gson? = null

        @JvmStatic
        fun makeHeaders(): Interceptor {

            return Interceptor { chain ->
                val original = chain.request()

                val request = original.newBuilder()
                        .header("Accept", ACCEPT_TYPE)
                        .header("Content-Type", CONTENT_TYPE)

                chain.proceed(request.build())
            }
        }

        @Synchronized
        @JvmStatic
        fun gson(): Gson {
            if (sharedGson == null) {
                sharedGson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .setLenient()
                    .registerTypeAdapterFactory(AutoValueAdapterFactory())
                    .create()
            }

            return sharedGson!!
        }
    }
}
