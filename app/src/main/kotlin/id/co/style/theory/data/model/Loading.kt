package id.co.style.theory.data.model

data class Loading(val type: String = "") {

    companion object {
        const val KEY_GRID_TWO_TYPE = "grid-2"
        const val KEY_GRID_FOUR_TYPE = "grid-4"
    }
}
