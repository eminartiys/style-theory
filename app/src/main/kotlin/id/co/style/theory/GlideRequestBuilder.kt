package id.co.style.theory

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import java.io.InputStream

class GlideRequestBuilder(loader: ModelLoader<GlideUrl, InputStream>): BaseGlideUrlLoader<String>(loader) {

    override fun getUrl(url: String, width: Int, height: Int, options: Options?): String {
        return transformRequest(url)
    }

    fun transformRequest(url: String): String {
        return url
    }

    override fun handles(model: String): Boolean {
        return true
    }

}
