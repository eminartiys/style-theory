package id.co.style.theory

import android.content.Context
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import java.io.InputStream

@GlideModule
class AppGlideModule: AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setDefaultRequestOptions(RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .format(DecodeFormat.PREFER_ARGB_8888))

        if (BuildConfig.DEBUG) {
            builder.setLogLevel(Log.VERBOSE)
        }

        builder.setMemoryCache(LruResourceCache(MEMORY_CACHE_SIZE.toLong()))
        builder.setBitmapPool(LruBitmapPool(BITMAP_POOL_SIZE.toLong()))
        builder.setDiskCache(InternalCacheDiskCacheFactory(context,
                "mabar-glide-cache", CACHE_SIZE.toLong()))
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry.prepend(String::class.java, InputStream::class.java,
                object : ModelLoaderFactory<String, InputStream> {
                    override fun build(multiFactory: MultiModelLoaderFactory):
                            ModelLoader<String, InputStream> {
                        return GlideRequestBuilder(multiFactory.build(GlideUrl::class.java,
                                InputStream::class.java))
                    }

                    override fun teardown() {

                    }
                })
    }

    companion object {
        const val CACHE_SIZE = 1024 * 1024
        private const val BITMAP_POOL_SIZE = 1024 * 1024 * 10
        private const val MEMORY_CACHE_SIZE = 1024 * 1024
    }

}
