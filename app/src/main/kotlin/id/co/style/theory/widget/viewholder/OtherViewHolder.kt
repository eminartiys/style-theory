package id.co.style.theory.widget.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View

class OtherViewHolder internal constructor(itemView: View): RecyclerView.ViewHolder(itemView)
