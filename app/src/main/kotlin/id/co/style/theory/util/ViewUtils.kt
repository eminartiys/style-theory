package id.co.style.theory.util

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.facebook.shimmer.ShimmerFrameLayout

class ViewUtils {

    companion object {

        fun skeletonLoading(shimmerFrameLayout: ShimmerFrameLayout?, loading: Boolean) {
            if (shimmerFrameLayout != null) {
                if (loading) {
                    shimmerFrameLayout.startShimmer()
                    shimmerFrameLayout.visibility = View.VISIBLE
                } else {
                    shimmerFrameLayout.stopShimmer()
                    shimmerFrameLayout.visibility = View.GONE
                }
            }

        }

        fun toastAboveNavBar(context: Context, message: String) {
            val toast = Toast.makeText(context,
                message, Toast.LENGTH_SHORT)
            toast.setGravity(
                Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL, 0, 220)
            return toast.show()
        }

    }
}