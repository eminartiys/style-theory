package id.co.style.theory.ui.color

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.style.theory.Injector
import id.co.style.theory.R
import id.co.style.theory.data.model.Color
import id.co.style.theory.data.model.Loading
import id.co.style.theory.event.ColorSelectedEvent
import id.co.style.theory.presenter.color.ColorPresenter
import id.co.style.theory.util.ViewUtils
import id.co.style.theory.widget.adapter.ItemAdapter
import id.co.style.theory.widget.contract.ItemClickListener
import id.co.style.theory.widget.contract.ScrollableViewContract
import id.co.style.theory.widget.viewhelper.RecyclerViewHelper
import kotlinx.android.synthetic.main.fragment_content.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class ColorFragment: Fragment(), ColorViewContract, ScrollableViewContract, ItemClickListener {

    @Inject lateinit var presenter: ColorPresenter

    private val adapter = ItemAdapter()

    private val retryClickListener = View.OnClickListener {
        adapter.clearItems()
        adapter.setLoadingItem(Loading(Loading.KEY_GRID_FOUR_TYPE))

        presenter.loadColors()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Injector.obtain(context!!).inject(this)
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.setView(this)
        initComponent()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        presenter.dispose()

        adapter.clearItems()
        feedRecyclerView?.adapter = null

    }

    private fun initComponent() {
        setUpSwipeRefresh()
        setUpRecyclerView()

        adapter.setLoadingItem(Loading(Loading.KEY_GRID_FOUR_TYPE))
        presenter.loadColors()

        adapter.setListener(this)
    }

    private fun setUpSwipeRefresh() {
        swipeRefreshLayout?.let {
            it.setOnRefreshListener {
                presenter.loadColors()
            }
        }

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorAccent)
    }

    private fun setUpRecyclerView() {
        feedRecyclerView?.let {
            val helper = RecyclerViewHelper(it, adapter)
            helper.setConfigLayout(context!!, RecyclerViewHelper.KEY_GRID_FOUR_LAYOUT_TYPE)
            helper.setConfigRecyclerView(true)
        }
    }

    override fun scrollToTop() {
        if (feedRecyclerView == null) return

        feedRecyclerView.smoothScrollToPosition(0)
    }

    override fun show(colors: List<Color>) {
        adapter.removeLoadItem()

        if (colors.isNullOrEmpty()) {
            swipeRefreshLayout.isRefreshing = false
            return
        }

        swipeRefreshLayout?.isRefreshing = false
        adapter.clearItems()
        adapter.setItems(colors)
    }

    override fun showError() {
        // Do nothing
        swipeRefreshLayout?.isRefreshing = false
        adapter.removeLoadItem()
        error?.showError(retryClickListener)
    }

    override fun hideError() {
        // Do nothing
        swipeRefreshLayout?.isRefreshing = false

        error?.let {
            if (error.isShown) error.hide()
        }
    }

    override fun onColorClicked(color: Color) {
        EventBus.getDefault().post(ColorSelectedEvent(color))

        ViewUtils.toastAboveNavBar(context!!, "Silahkan ke tab produk untuk melihat produk pilihan dengan warna ${color.name}")
    }

}
