package id.co.style.theory

import android.app.Application
import android.content.Context
import javax.inject.Inject


class App : Application() {

    private var objectGraph: AppGraph? = null

    @Inject lateinit var apiKeys: APIKeys

    override fun onCreate() {
        super.onCreate()

        // Dagger
        objectGraph = Injector.create(this)
        objectGraph!!.inject(this)

    }

    fun getInjector() : AppGraph {
        return objectGraph!!
    }

    companion object {
        fun get(context: Context): App {
            return context.applicationContext as App
        }
    }
}
