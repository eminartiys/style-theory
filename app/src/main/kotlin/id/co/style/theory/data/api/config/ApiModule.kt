package id.co.style.theory.data.api.config

import android.app.Application
import dagger.Module
import dagger.Provides
import id.co.style.theory.APIKeys
import id.co.style.theory.AppGlideModule.Companion.CACHE_SIZE
import id.co.style.theory.BuildConfig
import id.co.style.theory.data.api.service.ColorService
import id.co.style.theory.data.api.service.ProductService
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    internal fun provideEndpoint(): Endpoint = Endpoint.DEVELOPMENT

    @Provides
    @Singleton
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return loggingInterceptor
    }

    @Provides
    @Singleton
    internal fun provideCache(application: Application):
            Cache = Cache(File(application.cacheDir, "okhttp-cache"), CACHE_SIZE.toLong())

    @Provides
    @Singleton
    internal fun provideOkHttpClient(cache: Cache,
                                     loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {

        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.cache(cache)
        httpClientBuilder.addInterceptor(ApiConfig.makeHeaders())
        httpClientBuilder.addNetworkInterceptor(loggingInterceptor)
        httpClientBuilder.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClientBuilder.writeTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClientBuilder.readTimeout(TIMEOUT, TimeUnit.SECONDS)

        return httpClientBuilder.build()
    }

    @Provides
    @Singleton
    internal fun provideGsonConverterFactory():
            GsonConverterFactory = GsonConverterFactory.create(ApiConfig.gson())

    @Provides
    @Singleton
    internal fun provideRetrofit(okHttpClient: OkHttpClient,
                                 endpoint: Endpoint,
                                 apiKeys: APIKeys,
                                 converterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(endpoint.getUrl(apiKeys))
            .client(okHttpClient)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    internal fun provideProductService(retrofit: Retrofit): ProductService {
        return retrofit.create(ProductService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideColorService(retrofit: Retrofit): ColorService {
        return retrofit.create(ColorService::class.java)
    }

    companion object {
        private const val TIMEOUT: Long = 30
    }

}
