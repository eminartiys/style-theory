package id.co.style.theory.ui.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import id.co.style.theory.R
import id.co.style.theory.widget.viewpager.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity: AppCompatActivity() {

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponent()
    }

    private fun initComponent() {
        viewPagerAdapter = ViewPagerAdapter(this, viewpager, supportFragmentManager)
        viewPagerAdapter.clear()

        viewpager.adapter = viewPagerAdapter
        viewpager.offscreenPageLimit = 1

        tabs.setupWithViewPager(viewpager)

        val tabSelectedListener = object : TabLayout.ViewPagerOnTabSelectedListener(viewpager) {
            override fun onTabReselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                viewpager.setCurrentItem(tab.position, true)
            }
        }

        tabs.addOnTabSelectedListener(tabSelectedListener)
    }
}
