package id.co.style.theory.data.model

data class Product(val id: String = "",
                   val name: String = "",
                   val designer: List<Designer> = ArrayList(),
                   val gallery: List<String> = ArrayList(),
                   val featuredImage: String = "",
                   val price: Price = Price(),
                   val wishlist: Boolean = false,
                   val rating: Double = 0.0,
                   val totalReviewer: Int = 0,
                   val productUuid: String = "",
                   val favoriteId: Int = 0,
                   val sizeNotes: List<SizeNote> = ArrayList())