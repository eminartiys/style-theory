package id.co.style.theory.widget.viewhelper

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import id.co.style.theory.widget.adapter.ItemAdapter

class RecyclerViewHelper constructor(private val recyclerView: RecyclerView,
                                     private val recyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {

    private lateinit var layout: RecyclerView.LayoutManager

    fun setConfigRecyclerView(isNestedScrolling: Boolean) {
        recyclerView.apply {
            layoutManager = layout
            adapter = recyclerAdapter
            isNestedScrollingEnabled = isNestedScrolling
            onFlingListener = null
        }
    }

    fun setConfigLayout(context: Context, layoutCollection: String) {

        this.layout = when (layoutCollection) {
            KEY_GRID_TWO_LAYOUT_TYPE -> {
                val gridLayoutManager = GridLayoutManager(context, GRID_TWO)
                gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return when (recyclerAdapter.getItemViewType(position)) {
                            ItemAdapter.PRODUCT_THUMBNAIL_BOTTOM_VIEW_TYPE -> 1
                            else -> 2
                        }
                    }
                }

                gridLayoutManager
            }
            KEY_GRID_FOUR_LAYOUT_TYPE -> {
                val gridLayoutManager = GridLayoutManager(context, GRID_FOUR)
                gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return when (recyclerAdapter.getItemViewType(position)) {
                            ItemAdapter.COLOR_TEXT_BOTTOM_VIEW_TYPE -> 1
                            else -> 4
                        }
                    }
                }

                gridLayoutManager
            }
            else -> {
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            }
        }
    }

    companion object {
        const val KEY_GRID_FOUR_LAYOUT_TYPE = "grid-4"
        const val KEY_GRID_TWO_LAYOUT_TYPE = "grid-2"
        const val KEY_LIST_LAYOUT = "list"

        const val GRID_FOUR = 4
        const val GRID_TWO = 2
    }
}
