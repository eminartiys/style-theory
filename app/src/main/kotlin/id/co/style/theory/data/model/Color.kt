package id.co.style.theory.data.model

data class Color(val id: String = "",
                 val name: String = "",
                 val code: String = "")
