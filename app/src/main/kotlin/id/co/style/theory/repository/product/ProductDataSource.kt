package id.co.style.theory.repository.product

import id.co.style.theory.data.model.Feed
import io.reactivex.Single

interface ProductDataSource {

    fun getProducts(byPageNumber: Int, category: Int?, designer: Int?, colors: String?): Single<Feed>

}