package id.co.style.theory.widget.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import id.co.style.theory.data.model.Color
import id.co.style.theory.widget.contract.ItemClickListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_color_text_bottom.*
import java.lang.ref.WeakReference

class ColorTextBottomViewHolder
    internal constructor(itemView: View,
                         val listener: WeakReference<ItemClickListener>): RecyclerView.ViewHolder(itemView),
    View.OnClickListener, LayoutContainer {

    override val containerView: View
        get() = itemView

    private var item: WeakReference<Color>? = null
    private var itemPosition: Int = 0

    init {
        containerView.setOnClickListener(this)
    }

    fun bindView(color: Any, itemPosition: Int) {
        if (color !is Color) {
            return
        }

        this.item = WeakReference(color)
        this.itemPosition = itemPosition

        colorName.text = color.name

        if(color.code.isNotEmpty()) {
            colorBg.setCardBackgroundColor(android.graphics.Color.parseColor(color.code))
        }
    }

    override fun onClick(view: View) {
        if (listener.get() == null || item == null)
            return

        if (item!!.get() == null) return

        val color = item!!.get()!!
        val itemClickListener = listener.get()!!

        itemClickListener.onColorClicked(color)
    }
}
