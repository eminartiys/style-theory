package id.co.style.theory.widget.diffutil

import android.support.v7.util.DiffUtil
import id.co.style.theory.data.model.Color
import id.co.style.theory.data.model.Product


class ItemDiffCallback(private val oldList: List<Any>,
                       private val newList: List<Any>): DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        if (oldList[oldItemPosition] is Product && newList[newItemPosition] is Product) {
            return (oldList[oldItemPosition] as Product).id === (newList[newItemPosition] as Product).id
        } else if (oldList[oldItemPosition] is Color && newList[newItemPosition] is Color) {
            return (oldList[oldItemPosition] as Color).id === (newList[newItemPosition] as Color).id
        }

        return false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
