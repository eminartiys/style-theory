package id.co.style.theory.data.api.config

import id.co.style.theory.APIKeys

enum class Endpoint(val endpointName: String) {
    DEVELOPMENT("Development")
}

fun Endpoint.getUrl(apiKeys: APIKeys): String = apiKeys.getEndpointDevelopment()
