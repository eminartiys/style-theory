package id.co.style.theory.data.model

data class FilterOption(val length: Int = 0,
                        val sleeve: Int = 0,
                        val neckline: Int = 0,
                        val frameType: Int = 0)
