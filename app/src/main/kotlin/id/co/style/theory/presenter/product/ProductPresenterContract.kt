package id.co.style.theory.presenter.product

import id.co.style.theory.ui.product.ProductViewContract

interface ProductPresenterContract {

    fun setPageNumber(pageNumber: Int)

    fun setView(view: ProductViewContract)

    fun loadProduct(byCategory: Int?, byDesigner: Int?, byColor: String?)

    fun dispose()

}