package id.co.style.theory.data.api.config

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken

class AutoValueAdapterFactory: TypeAdapterFactory {
    @Suppress("UNCHECKED_CAST")
    override fun <T> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T>? {
        val rawType = type.rawType

        if (!rawType.isAnnotationPresent(AutoGson::class.java)) {
            return null
        }

        val packageName = rawType?.`package`?.name
        val length = packageName?.length ?: 0
        val className = rawType.name.substring(length + 1).replace('$', '_')
        val autoValueName = "$packageName.AutoValue_$className"

        try {
            val autoValueType = Class.forName(autoValueName)
            return gson.getAdapter(autoValueType) as TypeAdapter<T>
        } catch (e: ClassNotFoundException) {
            throw RuntimeException("Could not load AutoValue type $autoValueName", e)
        }

    }
}
