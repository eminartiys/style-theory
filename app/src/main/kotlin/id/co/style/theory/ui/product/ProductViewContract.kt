package id.co.style.theory.ui.product

import id.co.style.theory.data.model.Product

interface ProductViewContract {

    fun show(products: List<Product>, isLoadMore: Boolean)

    fun showError()

    fun hideError()
}