package id.co.style.theory.widget.viewhelper

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class LoadMoreOnScrollListener()
    : RecyclerView.OnScrollListener() {
    private var visibleItemsInOnePage = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        // scroll down
        if (dy > 0) {
            if (visibleItemsInOnePage == 0) {
                visibleItemsInOnePage = recyclerView.childCount
            }

            if (shouldLoadMore(recyclerView.layoutManager as LinearLayoutManager)) {
                loadMore()
            }
        }
    }

    private fun shouldLoadMore(layoutManager: LinearLayoutManager): Boolean {
        val pageThreshold = visibleItemsInOnePage * PAGE_MULTIPLIER
        val positionThreshold = layoutManager.itemCount - pageThreshold
        return layoutManager.findLastVisibleItemPosition() > Math.max(0, positionThreshold)
    }

    abstract fun loadMore()

    companion object {
        const val PAGE_MULTIPLIER = 2
    }

}
