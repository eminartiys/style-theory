package id.co.style.theory.data.model

data class SizeNote(val availableProductIds: List<String> = ArrayList(),
                    val sizeCategory: String = "",
                    val bustSize: Int = 0,
                    val waistSize: Int = 0,
                    val hipsSize: Int = 0,
                    val length: Int = 0,
                    val isNotificationOn: Boolean = false,
                    val isWishlisted: Boolean = false,
                    val status: String = "")
