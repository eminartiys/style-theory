package id.co.style.theory.repository.product

import id.co.style.theory.data.api.service.ProductService
import id.co.style.theory.data.model.Feed
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductRemoteDataSource @Inject
    constructor(private val productService: ProductService): ProductDataSource {

    override fun getProducts(byPageNumber: Int, category: Int?,
                             designer: Int?, colors: String?): Single<Feed> {
        val fetchNumPerPage = 10

        return productService.getProducts(fetchNumPerPage, byPageNumber, category, designer, colors)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}