package id.co.style.theory.ui.product

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.style.theory.Injector
import id.co.style.theory.R
import id.co.style.theory.data.model.Loading
import id.co.style.theory.data.model.Product
import id.co.style.theory.event.ColorSelectedEvent
import id.co.style.theory.presenter.product.ProductPresenter
import id.co.style.theory.widget.adapter.ItemAdapter
import id.co.style.theory.widget.contract.ScrollableViewContract
import id.co.style.theory.widget.viewhelper.LoadMoreOnScrollListener
import id.co.style.theory.widget.viewhelper.RecyclerViewHelper
import kotlinx.android.synthetic.main.fragment_content.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class ProductFragment: Fragment(), ProductViewContract, ScrollableViewContract {

    @Inject lateinit var presenter: ProductPresenter

    private val adapter = ItemAdapter()
    private var byCategory: Int? = null
    private var byDesigner: Int? = null
    private var byColor: String? = null

    private val retryClickListener = View.OnClickListener {
        adapter.clearItems()
        adapter.setLoadingItem(Loading(Loading.KEY_GRID_TWO_TYPE))

        presenter.loadProduct(byCategory, byDesigner, byColor)
    }

    private val loadMoreScrollListener = object : LoadMoreOnScrollListener() {
        override fun loadMore() {
            presenter.loadProduct(byCategory, byDesigner, byColor)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Injector.obtain(context!!).inject(this)
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }

        presenter.setView(this)
        initComponent()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }

        presenter.dispose()

        adapter.clearItems()
        feedRecyclerView?.removeOnScrollListener(loadMoreScrollListener)
        feedRecyclerView?.adapter = null

    }

    private fun initComponent() {
        setUpSwipeRefresh()
        setUpRecyclerView()

        adapter.setLoadingItem(Loading(Loading.KEY_GRID_TWO_TYPE))

        presenter.loadProduct(byCategory, byDesigner, byColor)
    }

    private fun setUpSwipeRefresh() {
        swipeRefreshLayout?.let {
            it.setOnRefreshListener {
                byColor = null
                presenter.setPageNumber(ProductPresenter.DEFAULT_PAGE_NUMBER)
                presenter.loadProduct(byCategory, byDesigner, byColor)
            }
        }

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorAccent)
    }

    private fun setupLoadMore() {
        feedRecyclerView?.addOnScrollListener(loadMoreScrollListener)
    }

    private fun setUpRecyclerView() {
        feedRecyclerView?.let {
            val helper = RecyclerViewHelper(it, adapter)
            helper.setConfigLayout(context!!, RecyclerViewHelper.KEY_GRID_TWO_LAYOUT_TYPE)
            helper.setConfigRecyclerView(true)
        }
    }

    override fun scrollToTop() {
        if (feedRecyclerView == null) return

        feedRecyclerView.smoothScrollToPosition(0)
    }


    override fun show(products: List<Product>, isLoadMore: Boolean) {
        adapter.removeLoadItem()

        if (products.isNullOrEmpty()) {
            swipeRefreshLayout.isRefreshing = false
            return
        }

        if (!isLoadMore) {
            setupLoadMore()
            swipeRefreshLayout?.isRefreshing = false
            adapter.clearItems()
            adapter.setItems(products)
        } else {
            adapter.addItems(products)
        }
    }

    override fun showError() {
        // Do nothing
        swipeRefreshLayout?.isRefreshing = false
        adapter.removeLoadItem()
        error?.showError(retryClickListener)
    }

    override fun hideError() {
        // Do nothing
        swipeRefreshLayout?.isRefreshing = false

        error?.let {
            if (error.isShown) error.hide()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRoomCreateEvent(event: ColorSelectedEvent) {
        byColor = event.color.id

        presenter.setPageNumber(ProductPresenter.DEFAULT_PAGE_NUMBER)
        presenter.loadProduct(byCategory, byDesigner, byColor)
    }
}
