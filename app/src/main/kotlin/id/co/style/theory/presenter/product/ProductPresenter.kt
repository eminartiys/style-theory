package id.co.style.theory.presenter.product

import android.util.Log
import id.co.style.theory.repository.product.ProductRepository
import id.co.style.theory.ui.product.ProductViewContract
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ProductPresenter @Inject
    constructor(private val repository: ProductRepository): ProductPresenterContract {

    private lateinit var view: ProductViewContract

    private var disposable: Disposable? = null
    private var pageNumber: Int = DEFAULT_PAGE_NUMBER
    private var endOfStream: Boolean = false

    override fun setPageNumber(pageNumber: Int) {
        this.pageNumber = pageNumber
    }

    override fun setView(view: ProductViewContract) {
        this.view = view
    }

    override fun loadProduct(byCategory: Int?, byDesigner: Int?, byColor: String?) {
        if (disposable != null) {
            Log.i(TAG, "Still loading feed, cancelling...")
            return
        }

        val isRefreshing = pageNumber == 1

        if (isRefreshing) {
            endOfStream = false
        } else if (endOfStream) {
            // Already end of stream
            return
        }

        view.hideError()

        disposable = repository.getProducts(pageNumber, byCategory, byDesigner, byColor)
            .subscribe({ feed ->
                dispose()

                pageNumber++

                if (isRefreshing) {
                    view.show(feed.products, false)
                } else {
                    view.show(feed.products, true)
                    endOfStream = feed.products.isEmpty()
                }
            }, { throwable ->
                dispose()
                view.showError()

                Log.e(TAG, throwable.message)
            })
    }

    override fun dispose() {
        disposable?.let {
            if (!disposable!!.isDisposed) {
                disposable!!.dispose()
            }
            disposable = null
        }
    }

    companion object {
        const val DEFAULT_PAGE_NUMBER = 1
        private const val TAG = "ProductPresenter"
    }

}