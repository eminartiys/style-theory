package id.co.style.theory.widget.contract

interface ScrollableViewContract {

    fun scrollToTop()
}
