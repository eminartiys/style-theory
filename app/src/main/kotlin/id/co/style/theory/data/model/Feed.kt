package id.co.style.theory.data.model

data class Feed(val products: List<Product> = ArrayList())
