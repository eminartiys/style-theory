package id.co.style.theory.event

import id.co.style.theory.data.model.Color

class ColorSelectedEvent constructor(val color: Color)
