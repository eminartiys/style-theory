package id.co.style.theory.widget.viewpager

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import id.co.style.theory.R
import id.co.style.theory.ui.product.ProductFragment


class ViewPagerAdapter(private val context: Context,
                       private val viewPager: ViewPager,
                       fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var currentFragment: android.support.v4.app.Fragment? = null

    // This is an alternative solution that provide at http://stackoverflow.com/a/16389033/2940459
    fun clear() {
        viewPager.removeAllViews()
        viewPager.adapter = null
        viewPager.adapter = this
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        if (currentFragment != `object`) {
            currentFragment = `object` as android.support.v4.app.Fragment
        }

        super.setPrimaryItem(container, position, `object`)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }

    override fun getItem(position: Int): android.support.v4.app.Fragment? {
        var fragment: android.support.v4.app.Fragment? = null
        if (position == 0) {
            fragment = ProductFragment()
        } else if (position == 1) {
            fragment = id.co.style.theory.ui.color.ColorFragment()
        }

        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = context.resources.getString(R.string.product)
        } else if (position == 1) {
            title = context.resources.getString(R.string.color)
        }
        return title
    }
}
