package id.co.style.theory.data

import dagger.Module
import dagger.Provides
import id.co.style.theory.data.api.config.ApiModule
import id.co.style.theory.data.api.service.ColorService
import id.co.style.theory.data.api.service.ProductService
import id.co.style.theory.repository.color.ColorDataSource
import id.co.style.theory.repository.color.ColorRemoteDataSource
import id.co.style.theory.repository.color.ColorRepository
import id.co.style.theory.repository.product.ProductDataSource
import id.co.style.theory.repository.product.ProductRemoteDataSource
import id.co.style.theory.repository.product.ProductRepository
import javax.inject.Singleton

@Module(includes = [ApiModule::class])
class DataModule {

    @Provides
    @Singleton
    internal fun provideProductRepository(remoteDataSource: ProductRemoteDataSource):
            ProductRepository = ProductRepository(remoteDataSource)

    @Provides
    @Singleton
    internal fun provideProductRemoteDataSource(service: ProductService):
            ProductDataSource = ProductRemoteDataSource(service)

    @Provides
    @Singleton
    internal fun provideColorRepository(remoteDataSource: ColorRemoteDataSource):
            ColorRepository = ColorRepository(remoteDataSource)

    @Provides
    @Singleton
    internal fun provideColorRemoteDataSource(service: ColorService):
            ColorDataSource = ColorRemoteDataSource(service)

}
