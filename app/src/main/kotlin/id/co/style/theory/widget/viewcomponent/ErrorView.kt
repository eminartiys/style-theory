package id.co.style.theory.widget.viewcomponent

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import id.co.style.theory.R
import kotlinx.android.synthetic.main.view_error.view.*

class ErrorView @JvmOverloads
constructor(context: Context,
            attrs: AttributeSet? = null): FrameLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_error, this)
    }

    fun showError(retryClickListener: OnClickListener?) {
        if (retryClickListener != null) {
            button.setOnClickListener(retryClickListener)
        }
        visibility = View.VISIBLE
    }

    fun hide() {
        visibility = View.GONE
    }

    override fun isShown(): Boolean {
        return visibility == View.VISIBLE
    }
}
