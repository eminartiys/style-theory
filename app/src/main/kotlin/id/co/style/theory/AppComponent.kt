package id.co.style.theory

import dagger.Component
import id.co.style.theory.data.DataModule
import javax.inject.Singleton

@Component(
        modules = [AppModule::class, DataModule::class]
)

@Singleton
interface AppComponent: AppGraph
