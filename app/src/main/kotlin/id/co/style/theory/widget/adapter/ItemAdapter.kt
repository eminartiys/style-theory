package id.co.style.theory.widget.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.style.theory.R
import id.co.style.theory.data.model.Color
import id.co.style.theory.data.model.Loading
import id.co.style.theory.data.model.Product
import id.co.style.theory.widget.contract.ItemClickListener
import id.co.style.theory.widget.diffutil.ItemDiffCallback
import id.co.style.theory.widget.viewholder.*
import java.lang.ref.WeakReference

class ItemAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = ArrayList<Any>()
    private lateinit var view: View

    var listener: WeakReference<ItemClickListener>? = null
        private set

    fun setListener(listener: ItemClickListener) {
        this.listener = WeakReference(listener)
    }

    fun setItems(list: List<Any>) {
        val result = DiffUtil.calculateDiff(ItemDiffCallback(items, list))

        this.items.clear()
        this.items.addAll(list)

        result.dispatchUpdatesTo(this)
    }

    fun addItems(list: List<Any>) {
        val count = itemCount
        this.items.addAll(list)

        notifyItemRangeInserted(count, list.size)
    }

    fun clearItems() {
        val count = itemCount
        this.items.clear()
        notifyItemRangeRemoved(0, count)
    }

    fun setLoadingItem(loading: Loading) {
        val position = items.size
        this.items.add(position, loading)

        notifyItemInserted(position)
    }

    fun removeLoadItem() {
        val position = items.size - 1

        if (position < 0 || items[position] !is Loading) return

        this.items.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun setView(parent: ViewGroup, viewType: Int) {
        this.view = when (viewType) {
            PRODUCT_THUMBNAIL_BOTTOM_VIEW_TYPE -> {
                LayoutInflater.from(parent.context).inflate(R.layout.view_product_thumbnail_text_bottom, parent, false)
            }
            COLOR_TEXT_BOTTOM_VIEW_TYPE -> {
                LayoutInflater.from(parent.context).inflate(R.layout.view_color_text_bottom, parent, false)
            }
            LOADING_GRIDTWO_VIEW_TYPE -> {
                LayoutInflater.from(parent.context).inflate(R.layout.view_load_grid_two_skeleton, parent, false)
            }
            LOADING_GRIDFOUR_VIEW_TYPE -> {
                LayoutInflater.from(parent.context).inflate(R.layout.view_load_grid_four_skeleton, parent, false)
            }
            else -> LayoutInflater.from(parent.context).inflate(R.layout.view_unsupported_item, parent, false)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        setView(parent, viewType)

        return when (viewType) {
            PRODUCT_THUMBNAIL_BOTTOM_VIEW_TYPE -> ProductThumbnailBottomViewHolder(this.view)
            COLOR_TEXT_BOTTOM_VIEW_TYPE -> ColorTextBottomViewHolder(this.view, listener!!)
            LOADING_GRIDTWO_VIEW_TYPE -> LoadingGridTwoViewHolder(this.view)
            LOADING_GRIDFOUR_VIEW_TYPE -> LoadingGridFourViewHolder(this.view)
            else -> OtherViewHolder(this.view)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProductThumbnailBottomViewHolder -> holder.bindView(items[position], position)
            is ColorTextBottomViewHolder -> holder.bindView(items[position], position)
            is LoadingGridTwoViewHolder -> holder.bindView()
            is LoadingGridFourViewHolder -> holder.bindView()
        }
    }

    override fun getItemViewType(position: Int): Int = when (this.items[position]) {
        is Product -> {
            PRODUCT_THUMBNAIL_BOTTOM_VIEW_TYPE
        }
        is Color -> {
            COLOR_TEXT_BOTTOM_VIEW_TYPE
        }
        is Loading -> {
            when {
                (this.items[position] as Loading).type == Loading.KEY_GRID_TWO_TYPE -> LOADING_GRIDTWO_VIEW_TYPE
                (this.items[position] as Loading).type == Loading.KEY_GRID_FOUR_TYPE -> LOADING_GRIDFOUR_VIEW_TYPE
                else -> -1
            }
        }
        else -> -1
    }

    companion object {
        const val LOADING_GRIDTWO_VIEW_TYPE = 1
        const val LOADING_GRIDFOUR_VIEW_TYPE = 2

        const val PRODUCT_THUMBNAIL_BOTTOM_VIEW_TYPE = 100
        const val COLOR_TEXT_BOTTOM_VIEW_TYPE = 200
    }
}