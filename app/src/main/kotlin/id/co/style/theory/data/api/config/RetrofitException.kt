package id.co.style.theory.data.api.config

import com.google.gson.JsonSyntaxException
import id.co.style.theory.data.api.config.RestError.Companion.ERRORINTERNALSERVER
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

enum class Kind {
    /** An IOException occurred while communicating to the server.  */
    NETWORK,
    /** A non-200 HTTP status code was received from the server.  */
    HTTP,
    /**
     * An internal error occurred while attempting to execute a request. It is best practice to
     * re-throw this exception so your application crashes.
     */
    UNEXPECTED
}

class RetrofitException(override val message: String,
                        val exception: Throwable?,
                        val url: String,
                        val response: Response<*>?,
                        val kind: Kind,
                        val retrofit: Retrofit?): RuntimeException(message, exception) {

    companion object {
        fun httpError(url: String, response: Response<*>, retrofit: Retrofit): RetrofitException {
            val message = "${response.code()} ${response.message()}"

            return RetrofitException(message, null, url, response, Kind.HTTP, retrofit)
        }

        fun networkError(exception: IOException): RetrofitException {
            return RetrofitException(exception.message ?: "", exception, "",
                    null, Kind.NETWORK, null)
        }

        fun unexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message ?: "", exception, "",
                    null, Kind.UNEXPECTED, null)
        }
    }

    @Throws(IOException::class)
    fun <T> getErrorBodyAs(type: Class<T>): T? {
        if (response?.errorBody() == null || retrofit == null) {
            return null
        }

        val converter = retrofit.responseBodyConverter<T>(type, arrayOfNulls<Annotation>(0))
        return converter.convert(response.errorBody()!!)
    }

    fun getRestError(exception: RetrofitException): RestError {
        return try {
            val restError = exception.getErrorBodyAs(RestError::class.java)

            when {
                restError?.message != null ->
                    RestError(response?.code() ?: ERRORINTERNALSERVER, restError.message)
                exception.kind == Kind.NETWORK ->
                    RestError(0, "Network Error")
                else ->
                    RestError(0, "Unexpected Error")
            }
        } catch (ex: JsonSyntaxException) {
            RestError(0, ex.message)
        } catch (ex: IOException) {
            RestError(0, ex.message)
        }

    }
}
