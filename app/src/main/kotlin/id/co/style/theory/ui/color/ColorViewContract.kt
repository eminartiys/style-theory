package id.co.style.theory.ui.color

import id.co.style.theory.data.model.Color

interface ColorViewContract {

    fun show(colors: List<Color>)

    fun showError()

    fun hideError()
}