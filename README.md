# style-theory-app
## Style Theory Lite Application

### There are 2 tabs in this application:
#### Tab 1 - Products
- This tab will show an infinite list of products in Style Theory, so the customers can discover products by scrolling through this tab. Once the customer reaches the bottom of the list, the application should fetch the product for the next page number.

#### Tab 2 - Colors
- This tab will show a list of color selection filter for Products. When the customer selects a color, the color should be indicated by a circle. And it will affect the product list (Tab 1). Product list will be refreshed automatically once a color has been selected, and it will show the only product with selected color only.

#### Feature:
- Show a list of products
- Show a list of colors
- Show a list of products based on colors when the user selects a color
- Show loading screen using skeleton
- Show error page when something bad happened 